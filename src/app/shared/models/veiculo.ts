export interface IVeiculo {
    id?: number;
    marca: string;
    modelo: string;
    valor: number;
    unidadesEmEstoque: number;
    ano: number;
    categoria: string;
    imagemUrl: string;
}







