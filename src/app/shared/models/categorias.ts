export interface ICategoria {
    id: number;
    descricao: string;
    nome: string;
}

export class Categoria implements ICategoria{
    public id: number; 
    public descricao: string;
    public nome: string;

    constructor(
        public categoria: ICategoria 
    ){
        this.id = categoria.id;
        this.nome = categoria.nome;
        this.descricao = categoria.descricao;
    }
       
    
}

export enum CategoriaEnum{
    MOTO = 'moto',
    CARRO = 'carro',
    CAMINHAO = 'caminhao'
}