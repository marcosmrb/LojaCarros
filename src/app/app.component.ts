import { Component } from '@angular/core';
import { VeiculoService } from './services/veiculo.service';
import { Categoria, CategoriaEnum } from './shared/models/categorias';
import { IVeiculo } from './shared/models';
import { CategoriaService } from './services/categoria.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  public mensagem: string;

  constructor(private veiculoService: VeiculoService) {
    this.exibeMensagemDoServico();
  }

  exibeMensagemDoServico() {
    console.log(this.veiculoService.getVeiculos(CategoriaEnum.MOTO).subscribe((data: IVeiculo[]) => {
      console.log(data );
    }));
  }

  // exibeMensagemDoServico() {
  //   console.log(this.veiculoService.createVeiculo(
  //     {
  //       marca: 'Honda',
  //       modelo: 'CB600F Hornet',
  //       ano: 2013,
  //       categoria: CategoriaEnum.MOTO,
  //       valor: 28990,
  //       imagemUrl: 'http:\\..',
  //       unidadesEmEstoque: 0
  //     } as IVeiculo
  //   ).subscribe((data: IVeiculo) => {
  //     console.log(data );
  //   }));
  // }

  
  // exibeMensagemDoServico() {
  //   console.log(this.veiculoService.getCategoriasVeiculo().subscribe((data: Categoria[]) => {
  //     console.log(data );
  //   }));
  // }
}
