import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take, map, distinctUntilChanged, retry } from 'rxjs/operators';
import { CategoriaEnum } from '../shared/models/categorias';
import { IVeiculo } from '../shared/models';
import { HttpBase } from './http-base';

@Injectable({
  providedIn: 'root'
})
export class VeiculoService extends HttpBase {
  constructor(private _httpClient: HttpClient) {
    super('veiculos');
  }

  

  getVeiculos(nomeCategoria: CategoriaEnum): Observable<IVeiculo[]>{
    const parametros = new HttpParams()
    .set('categoria', nomeCategoria)
    .set('unidadesEmEstoque_gte', '1');

    return this._httpClient.get<IVeiculo[]>(`${this.UrlBase}`, {params: parametros})
      .pipe(
        take(1), 
        retry(3), //Quantidade de tentativas do request
        distinctUntilChanged() //Evita multiplas request para mesma rota
      )
  }

  getVeiculoById(idVeiculo:number): Observable<IVeiculo>{
    return this._httpClient.get<IVeiculo>(`${this.UrlBase}/${idVeiculo}`)
  }

  createVeiculo(veiculo: IVeiculo){
    return this._httpClient.post<any>(`${this.UrlBase}`, veiculo)
  }
}
